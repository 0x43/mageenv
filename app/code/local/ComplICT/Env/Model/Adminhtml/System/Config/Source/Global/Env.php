<?php
class ComplICT_Env_Model_Adminhtml_System_Config_Source_Global_Env
{
    /**
     * Options getter
     * 
     * @return array
     */
    public function toOptionArray()
    {
    	$cn = get_class(Mage::helper('env/config'));
    	$envs = $cn::$ENV;
    	$h = Mage::helper('env');
    	$r = array();
    	foreach($envs as $k => $v)
    		$r[] = array('value' => $k, 'label'=> $h->__(ucfirst($v)));
    	return $r;
    }
}
