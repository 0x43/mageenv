<?php
class ComplICT_Env_Model_Adminhtml_Observer
{
	/**
     * Flag to stop observer executing more than once
     *
     * @var static bool
     */
    static protected $_singletonFlag = false;
    
    public function adminhtml_controller_action_predispatch_start(Varien_Event_Observer $observer)
    {
    	if (self::$_singletonFlag)
			return;
		self::$_singletonFlag = true;
    	if(Mage::app()->getResponse()->isRedirect())
    		return;
    	$h = Mage::helper('env/config');
    	if($h->showNoticeInAdminHtml())
	    	// make sure the messages are unique
	    	Mage::getSingleton('adminhtml/session')->addUniqueMessages(
	    		Mage::getSingleton('core/message')->notice(
	    			"Current ENV: ".$h->__(ucfirst($h->getEnvLabel()))
	    		)
	    	);
    }
}
