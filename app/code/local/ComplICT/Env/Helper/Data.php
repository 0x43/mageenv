<?php
class ComplICT_Env_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function isLive() {
		return Mage::helper('env/config')->isLive();	
	}
	
	public function treatAsLive() {
		return Mage::helper('env/config')->treatAsLive();
	}
	
	public function getEnv() {
		return Mage::helper('env/config')->getEnvLabel();
	}
}
	 
