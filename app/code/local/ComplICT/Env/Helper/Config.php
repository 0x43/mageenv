<?php
class ComplICT_Env_Helper_Config extends Mage_Core_Helper_Abstract
{
	const XML_PATH_GLOBAL_ENV                    = 'env/global/env';
 	const XML_PATH_TREAT_AS_LIVE                 = 'env/treat_as_live';
 	const XML_PATH_SHOW_NOTICE_ADMINHTML         = 'env/show_notice_adminhtml';
 	const XML_PATH_SHOW_NOTICE_ADMINHTML_ENABLED = 'env/show_notice_adminhtml/enabled';
 	
 	/**
 	 *	Based on http://en.wikipedia.org/wiki/Development_environment_%28software_development_process%29
 	 */
 	public static $ENV = array(		
    	 1 => 'production',
    	10 => 'staging',
    	20 => 'uat',
    	30 => 'test',
    	40 => 'qa',
    	50 => 'integration',
    	60 => 'dev',
    	70 => 'local',
    );

    protected function _getConfig($path) {
    	return Mage::getStoreConfig($path);
    }
    
    public function getEnv() {
    	return $this->_getConfig(self::XML_PATH_GLOBAL_ENV);
    }
    
    public function getEnvLabel() {
    	return self::$ENV[$this->getEnv()];
    }
    
    public function isLive($treatAs = true) {
    	if($threadAs) 
    		return $this->treatAsLive();
    	return $this->_getConfig(self::XML_PATH_GLOBAL_ENV) == 1;
    }
    
    public function treatAsLive() {
    	if($this->isLive(false))
    		return true;
    	$list = $this->_getConfig(self::XML_PATH_TREAT_AS_LIVE);
		return (bool) $list[self::$ENV[$this->getEnv()]];
	}
	
	public function showNoticeInAdminHtml() {
		if(!$this->_getConfig(self::XML_PATH_SHOW_NOTICE_ADMINHTML_ENABLED))
			return false;
    	$list = $this->_getConfig(self::XML_PATH_SHOW_NOTICE_ADMINHTML);
		return (bool) $list[self::$ENV[$this->getEnv()]];
	}
}
	 
